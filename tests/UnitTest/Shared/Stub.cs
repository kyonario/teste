namespace UnitTest.Shared;

public record Stub(string Value);
