using System.Diagnostics.CodeAnalysis;

namespace DotChain.Models;

[ExcludeFromCodeCoverage]
public record BlockStarData
{
    public string Owner { get; init; } = "";
    public BlockStar BlockStar { get; init; } = new ();
}

[ExcludeFromCodeCoverage]
public record BlockStar
{
    public int Rating { get; init; }
    public string Description { get; init; } = "";
}