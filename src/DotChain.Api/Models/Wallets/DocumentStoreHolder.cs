using System.Security.Cryptography.X509Certificates;
using Raven.Client.Documents;
using Raven.Client.Documents.Operations;
using Raven.Client.Exceptions;
using Raven.Client.Exceptions.Database;
using Raven.Client.ServerWide;
using Raven.Client.ServerWide.Operations;

namespace DotChain.Api.Models.Wallets;

// The `DocumentStoreHolder` class holds a single Document Store instance.
public static class DocumentStoreHolder
{
    // Use Lazy<IDocumentStore> to initialize the document store lazily. 
    // This ensures that it is created only once - when first accessing the public `Store` property.
    private static readonly Lazy<IDocumentStore> _store = new(CreateStore);

    public static IDocumentStore Store => _store.Value;

    private static IDocumentStore CreateStore()
    {
        var store = new DocumentStore()
            {
                Urls = new[] { "http://localhost:8080" },

                // Set conventions as necessary (optional)
                Conventions =
                {
                    MaxNumberOfRequestsPerSession = 10,
                    UseOptimisticConcurrency = true
                },

                // Define a default database (optional)
                Database = "dotchain",

                // Define a client certificate (optional)
                //Certificate = new X509Certificate2("C:\\path_to_your_pfx_file\\cert.pfx"),

                // Initialize the Document Store
            }
            .Initialize()
            .EnsureDatabaseExists("dotchain");


        return store;
    }

    private static IDocumentStore EnsureDatabaseExists(this IDocumentStore store, string database = null, bool createDatabaseIfNotExists = true)
    {
        database = database ?? store.Database;

        if (string.IsNullOrWhiteSpace(database))
            throw new ArgumentException("Value cannot be null or whitespace.", nameof(database));

        try
        {
            store.Maintenance.ForDatabase(database).Send(new GetStatisticsOperation());
            return store;
        }
        catch (DatabaseDoesNotExistException)
        {
            if (createDatabaseIfNotExists == false)
                throw;

            try
            {
                store.Maintenance.Server.Send(new CreateDatabaseOperation(new DatabaseRecord(database)));
                return store;
            }
            catch (ConcurrencyException)
            {
                return store;
                // The database was already created before calling CreateDatabaseOperation
            }
        }
    }
}